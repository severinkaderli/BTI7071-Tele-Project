programs = ./build/binary/parser ./build/binary/hub ./build/binary/switch ./build/binary/vswitch ./build/binary/arp ./build/binary/router
CFLAGS = -O0 -g -Wall

all: ./build/binary/network-driver $(programs)

./build/binary/network-driver: ./src/network-driver.c ./src/glab.h
	mkdir -p build/binary
	gcc -g -O0 -Wall -o ./build/binary/network-driver ./src/network-driver.c

$(programs): ./build/binary/%: ./src/%.c ./src/glab.h ./src/loop.c ./src/print.c ./src/utils.c
	mkdir -p build/binary
	gcc $(CFLAGS) $< -o $@ ./src/utils.c

clean:
	rm -rf ./build
