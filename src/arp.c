/*
     This file (was) part of GNUnet.
     Copyright (C) 2018 Christian Grothoff

     GNUnet is free software: you can redistribute it and/or modify it
     under the terms of the GNU Affero General Public License as published
     by the Free Software Foundation, either version 3 of the License,
     or (at your option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     Affero General Public License for more details.

     You should have received a copy of the GNU Affero General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file arp.c
 * @brief ARP tool
 * @author Christian Grothoff
 */
#include "glab.h"
#include "mac_address.h"
#include "arp_table_entry.h"
#include "ethernet_header.h"
#include "arp_ethernet_header_ipv4.h"
#include "interface_ipv4.h"
#include <arpa/inet.h>
#include <stdbool.h>
#include "utils.h"
#include "crc.c"


/**
 * Number of available contexts.
 */
static unsigned int num_ifc;

/**
 * All the contexts.
 */
static struct Interface *gifc;

#define ARP_ETHERTYPE (uint16_t)0x0806
#define ARP_OP_REQUEST (uint16_t)0x0001
#define ARP_OP_RESPONSE (uint16_t)0x0002
#define HTYPE_ETHERNET (uint16_t)0x0001
#define PTYPE_IPV4 (uint16_t)0x0800
#define CRC32_LEN 4
//#define ARP_FRAME_LEN sizeof(struct EthernetHeader) + sizeof(struct ArpHeaderEthernetIPv4) + CRC32_LEN
#define ARP_FRAME_LEN sizeof(struct EthernetHeader) + sizeof(struct ArpHeaderEthernetIPv4)

#define TABLE_SIZE 128
static struct ARPTableEntry arp_table[TABLE_SIZE];

static struct in_addr pending[TABLE_SIZE];

#define OWN_ADDR_TABLE_SIZE 16
static struct in_addr own_addresses[OWN_ADDR_TABLE_SIZE];

/**
 * @return - true if the supplied in_addr is one of our own addresses.
 */
static bool is_own_addr(struct in_addr *ip) {
    for(int i = 0; i < OWN_ADDR_TABLE_SIZE; i++) {
        if(compare_ip(ip, &own_addresses[i]) == 0) {
            return true;
        }
    }
    return false;
}

static struct ARPTableEntry *find_in_table(struct in_addr *ip) {
    for (int i = 0; i < TABLE_SIZE; i++) {
        if (compare_ip(ip, &arp_table[i].ipAddress) == 0) {
            return &arp_table[i];
        }
    }
    return NULL;
}

static bool in_table(struct in_addr *ip) {
    return find_in_table(ip) != NULL;
}

static bool add_pending(struct in_addr *ip) {
    for(int i = 0; i < TABLE_SIZE; i++) {
        if(pending[i].s_addr == NULL) {
            pending[i] = *ip;
            return true;
        }
    }
    printerr("Pending table full!");
    return false;
}

static bool remove_pending(struct in_addr *ip) {
    for(int i = 0; i < TABLE_SIZE; i++) {
        if(compare_ip(ip, &pending[i]) == 0) {
            pending[i].s_addr = NULL;
            return true;
        }
    }
    return false;
}

static void print_arp_entry(struct in_addr ip, struct MacAddress mac, char *name) {
    /*
   print_ip(&ip);
   print(" -> ");
   print_mac(&mac);
   print(" (%s)\n", name);
     */
   print("%s -> %s (%s)\n", to_str_ip(&ip), to_str_mac(&mac), name);
}

static void print_table() {
    for (int i = 0; i < TABLE_SIZE; i++) {
        struct ARPTableEntry entry = arp_table[i];
        if(entry.ifc_num != 0 && !is_own_addr(&entry.ipAddress)) {
            print_arp_entry(entry.ipAddress, entry.macAddress, gifc[entry.ifc_num - 1].name);
        }
    }
}

static void write_to_table(struct ARPTableEntry *entry) {
    for (int i = 0; i < TABLE_SIZE; i++) {
        if (arp_table[i].ifc_num == 0) { // no entry at this location
            arp_table[i] = *entry;
            return;
        }
    }

    unsigned int oldest_index = 0;
    unsigned long oldest_time = 2147483648; // 2<<31, but the compiler is happy
    for (int i = 0; i < TABLE_SIZE; i++) {
        if (arp_table[i].ifc_num == 0) {
            if (arp_table[i].ts < oldest_time) {
                oldest_time = arp_table[i].ts;
                oldest_index = i;
            }
        }
    }
    arp_table[oldest_index] = *entry;
}

/**
 * Forward @a frame to interface @a dst.
 *
 * @param dst target interface to send the frame out on
 * @param frame the frame to forward
 * @param frame_size number of bytes in @a frame
 */
static void forward_to(struct Interface *dst, const void *frame, size_t frame_size) {
  char iob[frame_size + sizeof(struct GLAB_MessageHeader)];
  struct GLAB_MessageHeader hdr;

  if (frame_size > dst->mtu)
    abort();
  hdr.size = htons(sizeof(iob));
  hdr.type = htons(dst->ifc_num);
  memcpy(iob, &hdr, sizeof(hdr));
  memcpy(&iob[sizeof(hdr)], frame, frame_size);
  write_all(STDOUT_FILENO, iob, sizeof(iob));
}

static bool is_arp(struct EthernetHeader *eh) {
    return ARP_ETHERTYPE == htons(eh->tag);
}

static bool is_arp_request(struct ArpHeaderEthernetIPv4 *header) {
    return ntohs(header->oper) == 1;
}

static bool is_arp_response(struct ArpHeaderEthernetIPv4 *header) {
    return ntohs(header->oper) == 2;
}

static void update_arp_cache(struct ArpHeaderEthernetIPv4 *arp_header, uint16_t ifc_num) {
    // if it's already in the table
    if(in_table(&arp_header->sender_pa)) {
        struct ARPTableEntry *existing = find_in_table(&arp_header->sender_pa);
        // and the mac address is different
        if(compare_mac(&existing->macAddress, &arp_header->sender_ha) != 0) {
            // update it
            existing->macAddress = arp_header->sender_ha;
            existing->ifc_num = ifc_num;
            existing->ts = (unsigned long)time(NULL);
            existing->ipAddress = arp_header->sender_pa;
        }
    } else {
        struct ARPTableEntry newEntry;
        newEntry.macAddress = arp_header->sender_ha;
        newEntry.ipAddress = arp_header->sender_pa;
        newEntry.ts = (unsigned long)time(NULL);
        newEntry.ifc_num = ifc_num;
        write_to_table(&newEntry);
    }
}

static void* build_arp_frame(
        uint16_t oper,
        struct MacAddress sha,
        struct in_addr spa,
        struct MacAddress tha,
        struct in_addr tpa) {
    struct EthernetHeader eh;
    eh.src = sha;
    eh.dst = tha;
    eh.tag = htons(ARP_ETHERTYPE);

    struct ArpHeaderEthernetIPv4 ah;
    ah.htype = htons(HTYPE_ETHERNET);
    ah.ptype = htons(PTYPE_IPV4);
    ah.hlen = MAC_ADDR_SIZE;
    ah.plen = 4;
    ah.oper = htons(oper);
    ah.sender_ha = sha;
    ah.sender_pa = spa;
    if(oper == ARP_OP_REQUEST) {
        struct MacAddress *nullMac = malloc(MAC_ADDR_SIZE);
        memset(nullMac, 0, MAC_ADDR_SIZE);
        ah.target_ha = *nullMac;
        free(nullMac);
    } else {
        ah.target_ha = tha;
    }

    ah.target_pa = tpa;

    size_t eh_size = sizeof(eh);
    size_t ah_size = sizeof(ah);

    /*
    void* frame = malloc(eh_size + ah_size + CRC32_LEN);
    memcpy(frame, &eh, eh_size);
    memcpy(frame + eh_size, &ah, ah_size);
    memset(frame + eh_size + ah_size, NULL, CRC32_LEN);

    uint32_t crc = crc32(0, frame, ARP_FRAME_LEN);
    crc = crc32(crc, frame, ARP_FRAME_LEN);
    memcpy(frame + eh_size + ah_size, &crc, CRC32_LEN);
     */

    void* frame = malloc(eh_size + ah_size);
    memcpy(frame, &eh, eh_size);
    memcpy(frame + eh_size, &ah, ah_size);

    return frame;
}

static void respond_to_arp_request(struct Interface *ifc, struct ArpHeaderEthernetIPv4* arp_header) {

    void *frame = build_arp_frame(
            ARP_OP_RESPONSE,
            ifc->mac,
            ifc->ip,
            arp_header->sender_ha,
            arp_header->sender_pa
            );

    forward_to(ifc, frame, ARP_FRAME_LEN);
}

/**
 * Parse and process frame received on @a ifc.
 *
 * @param ifc interface we got the frame on
 * @param frame raw frame data
 * @param frame_size number of bytes in @a frame
 */
static void parse_frame(struct Interface *ifc, const void *frame, size_t frame_size) {
  struct EthernetHeader eh;

  if (frame_size < sizeof(eh)) {
    fprintf(stderr, "Malformed frame\n");
    return;
  }
  memcpy(&eh, frame, sizeof(eh));

  if(is_arp(&eh)) {
      struct ArpHeaderEthernetIPv4 arp_header;
      memcpy(&arp_header, frame + sizeof(eh), sizeof(arp_header));

      if(is_arp_request(&arp_header)) {
          if(is_own_addr(&arp_header.target_pa)) {
              respond_to_arp_request(ifc, &arp_header);
          }
      } else if(is_arp_response(&arp_header)) {
          update_arp_cache(&arp_header, ifc->ifc_num);
          if(remove_pending(&arp_header.sender_pa)) {
              print_arp_entry(arp_header.sender_pa, arp_header.sender_ha, ifc->name);
          }
      }
  } else {
      // NOP - not an arp packet
  }
}

/**
 * Process frame received from @a interface.
 *
 * @param interface number of the interface on which we received @a frame
 * @param frame the frame
 * @param frame_size number of bytes in @a frame
 */
static void handle_frame(uint16_t interface, const void *frame, size_t frame_size) {
  if (interface > num_ifc)
    abort();
  parse_frame(&gifc[interface - 1], frame, frame_size);
}

static void send_arp_request(struct in_addr ip, struct Interface *ifc) {
    struct MacAddress *broadcast = malloc(sizeof(struct MacAddress));
    memset(broadcast, 0xff, MAC_ADDR_SIZE);

    void* frame = build_arp_frame(
            ARP_OP_REQUEST,
            ifc->mac,
            ifc->ip,
            *broadcast,
            ip
            );

    forward_to(ifc, frame, ARP_FRAME_LEN);
}

/**
 * The user entered an "arp" command.  The remaining
 * arguments can be obtained via 'strtok()'.
 */
static void process_cmd_arp() {
  const char *tok = strtok(NULL, " ");
  struct in_addr v4;
  struct Interface *ifc;

  if (NULL == tok) {
    print_table();
    return;
  }
  if (1 != inet_pton(AF_INET, tok, &v4)) {
    fprintf(stderr, "`%s' is not a valid IPv4 address\n", tok);
    return;
  }
  tok = strtok(NULL, " ");
  if (NULL == tok) {
    fprintf(stderr, "No network interface provided\n");
    return;
  }
  ifc = NULL;
  for (unsigned int i = 0; i < num_ifc; i++) {
    if (0 == strcasecmp(tok, gifc[i].name)) {
      ifc = &gifc[i];
      break;
    }
  }
  if (NULL == ifc) {
    fprintf(stderr, "Interface `%s' unknown\n", tok);
    return;
  }

  if(in_table(&v4)) {
      struct ARPTableEntry *entry = find_in_table(&v4);
      print_arp_entry(entry->ipAddress, entry->macAddress, gifc[entry->ifc_num - 1].name);
  } else {
      send_arp_request(v4, ifc);
      add_pending(&v4);
  }
}

/**
 * Parse network specification in @a net, initializing @a ifc.
 * Format of @a net is "IPV4:IP/NETMASK".
 *
 * @param ifc[out] interface specification to initialize
 * @param arg interface specification to parse
 * @return 0 on success
 */
static int parse_network(struct Interface *ifc, const char *net) {
  const char *tok;
  char *ip;
  unsigned int mask;

  if (0 != strncasecmp(net, "IPV4:", strlen("IPV4:"))) {
    fprintf(stderr, "Interface specification `%s' does not start with `IPV4:'\n", net);
    return 1;
  }
  net += strlen("IPV4:");
  tok = strchr(net, '/');
  if (NULL == tok) {
    fprintf(stderr, "Error in interface specification `%s': lacks '/'\n", net);
    return 1;
  }
  ip = strndup(net, tok - net);
  if (1 != inet_pton(AF_INET, ip, &ifc->ip)) {
    fprintf(stderr, "IP address `%s' malformed\n", ip);
    free(ip);
    return 1;
  }
  free(ip);
  tok++;
  if (1 != sscanf(tok, "%u", &mask)) {
    fprintf(stderr, "Netmask `%s' malformed\n", tok);
    return 1;
  }
  if (mask > 32) {
    fprintf(stderr, "Netmask invalid (too large)\n");
    return 1;
  }
  ifc->netmask.s_addr = htonl(~(uint32_t)((1LLU << (32 - mask)) - 1LLU));
  return 0;
}

/**
 * Parse interface specification @a arg and update @a ifc.  Format is
 * "IFCNAME[IPV4:IP/NETMASK]=MTU".  The "=MTU" is optional.
 *
 * @param ifc[out] interface specification to initialize
 * @param arg interface specification to parse
 * @return 0 on success
 */
static int parse_cmd_arg(struct Interface *ifc, const char *arg) {
  const char *tok;
  char *nspec;

  ifc->mtu = 1500; /* default in case unspecified */
  tok = strchr(arg, '[');
  if (NULL == tok) {
    fprintf(stderr, "Error in interface specification: lacks '['");
    return 1;
  }
  ifc->name = strndup(arg, tok - arg);
  arg = tok + 1;
  tok = strchr(arg, ']');
  if (NULL == tok) {
    fprintf(stderr, "Error in interface specification: lacks ']'");
    return 1;
  }
  nspec = strndup(arg, tok - arg);
  if (0 != parse_network(ifc, nspec)) {
    free(nspec);
    return 1;
  }
  free(nspec);
  arg = tok + 1;
  if ('=' == arg[0]) {
    unsigned int mtu;

    if (1 != (sscanf(&arg[1], "%u", &mtu))) {
      fprintf(stderr, "Error in interface specification: MTU not a number\n");
      return 1;
    }
    if (mtu < 400) {
      fprintf(stderr, "Error in interface specification: MTU too small\n");
      return 1;
    }
  }
  return 0;
}

/**
 * Handle control message @a cmd.
 *
 * @param cmd text the user entered
 * @param cmd_len length of @a cmd
 */
static void handle_control(char *cmd, size_t cmd_len) {
  const char *tok;

  cmd[cmd_len - 1] = '\0';
  tok = strtok(cmd, " ");
  if (0 == strcasecmp(tok, "arp"))
    process_cmd_arp();
  else
    fprintf(stderr, "Unsupported command `%s'\n", tok);
}

/**
 * Handle MAC information @a mac
 *
 * @param ifc_num number of the interface with @a mac
 * @param mac the MAC address at @a ifc_num
 */
static void handle_mac(uint16_t ifc_num, const struct MacAddress *mac) {
  if (ifc_num > num_ifc)
    abort();
  gifc[ifc_num - 1].mac = *mac;
}

#include "loop.c"

/**
 * Launches the arp tool.
 *
 * @param argc number of arguments in @a argv
 * @param argv binary name, followed by list of interfaces to switch between
 * @return not really
 */
int main(int argc, char **argv) {
  struct Interface ifc[argc];

  memset(arp_table, 0, sizeof(arp_table));
  memset(pending, 0, sizeof(pending));
  memset(own_addresses, 0, sizeof(own_addresses));

  memset(ifc, 0, sizeof(ifc));
  num_ifc = argc - 1;
  gifc = ifc;
  for (unsigned int i = 1; i < argc; i++) {
    struct Interface *p = &ifc[i - 1];

    ifc[i - 1].ifc_num = i;
    if (0 != parse_cmd_arg(p, argv[i]))
      abort();
    struct ARPTableEntry arpTableEntry;
    arpTableEntry.ifc_num = p->ifc_num;
    arpTableEntry.macAddress = p->mac;
    arpTableEntry.ipAddress = p->ip;
    arpTableEntry.ts = (unsigned long)time(NULL);
    write_to_table(&arpTableEntry);
    own_addresses[i - 1] = p->ip;
  }
  loop();
  for (unsigned int i = 1; i < argc; i++)
    free(ifc[i - 1].name);
  return 0;
}
