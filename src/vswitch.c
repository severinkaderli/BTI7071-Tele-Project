/*
     This file (was) part of GNUnet.
     Copyright (C) 2018 Christian Grothoff

     GNUnet is free software: you can redistribute it and/or modify it
     under the terms of the GNU Affero General Public License as published
     by the Free Software Foundation, either version 3 of the License,
     or (at your option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     Affero General Public License for more details.

     You should have received a copy of the GNU Affero General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file vswitch.c
 * @brief Ethernet switch
 * @author Christian Grothoff
 */
#include "ethernet_header.h"
#include "glab.h"
#include "interface.h"
#include "mac_address.h"
#include "q.h"
#include "switching_table_entry.h"
#include "utils.h"
#include <stdbool.h>

/**
 * Value used to indicate "no VLAN" (or no more VLANs).
 */
#define NO_VLAN (-1)

/**
 * Which VLAN should we assume for untagged frames on
 * interfaces without any specified tag?
 */
#define DEFAULT_VLAN 0

#define TABLE_SIZE 128
static struct SwitchingTableEntry switching_table[MAX_VLANS + 1][TABLE_SIZE];

/**
 * Number of available contexts.
 */
static unsigned int num_ifc;

/**
 * All the contexts.
 */
static struct Interface *gifc;

static bool has_vlan(struct Interface *ifc, int16_t vlan) {
  //printerr("has_vlan: vlan=%d, ifc_nr=%d, ifc=%s\n", vlan, ifc->ifc_num, ifc->ifc_name);
  for (int i = 0; i < MAX_VLANS; i++) {
    if (ifc->tagged_vlans[i] == vlan)
      return true;
    if (ifc->tagged_vlans[i] == NO_VLAN)
      return false;
  }

  return false;
}

static struct SwitchingTableEntry *find_in_table(struct MacAddress *mac, int16_t vlan) {
  for (int i = 0; i < TABLE_SIZE; i++) {
    if (compare_mac(mac, &switching_table[vlan][i].macAddress) == 0) {
      return &switching_table[vlan][i];
    }
  }
  return NULL;
}

static bool in_table(struct MacAddress *mac, int16_t vlan) {
  for (int i = 0; i < TABLE_SIZE; i++) {
    if (compare_mac(mac, &switching_table[vlan][i].macAddress) == 0) {
      return true;
    }
  }
  return false;
}

static void write_to_table(struct SwitchingTableEntry *entry, int16_t vlan) {
  for (int i = 0; i < TABLE_SIZE; i++) {
    if (switching_table[vlan][i].port_no == 0) { // no entry at this location
      switching_table[vlan][i] = *entry;
      return;
    }
  }

  unsigned int oldest_index = 0;
  unsigned long oldest_time = 1 << 31;
  for (int i = 0; i < TABLE_SIZE; i++) {
    if (switching_table[vlan][i].port_no == 0) {
      if (switching_table[vlan][i].ts < oldest_time) {
        oldest_time = switching_table[vlan][i].ts;
        oldest_index = i;
      }
    }
  }
  switching_table[vlan][oldest_index] = *entry;
}

/**
 * Forward @a frame to interface @a dst.
 *
 * @param dst target interface to send the frame out on
 * @param frame the frame to forward
 * @param frame_size number of bytes in @a frame
 */
static void forward_to(struct Interface *dst, const void *frame, size_t frame_size) {
  char iob[frame_size + sizeof(struct GLAB_MessageHeader)];
  struct GLAB_MessageHeader hdr;

  hdr.size = htons(sizeof(iob));
  hdr.type = htons(dst->ifc_num);
  memcpy(iob, &hdr, sizeof(hdr));
  memcpy(&iob[sizeof(hdr)], frame, frame_size);
  write_all(STDOUT_FILENO, iob, sizeof(iob));
}

static int16_t extract_vid(struct Q *qh) { return (qh->tci) & 0x0fff; }

static bool contains(int16_t *arr, int16_t entry) {
  for (int i = 0; i < MAX_VLANS; i++) {
    if (arr[i] == entry) {
      return true;
    }
    if (arr[i] == NO_VLAN) {
      break;
    }
  }

  return false;
}

static void *tag_frame(int16_t vlan, const void *frame, size_t frame_size) {
  struct Q qh = {
      .tpid = htons(Q_TAG),
      .tci = htons(vlan) // pcp,dei=0
  };
  //printerr("Size before tagging: %ld\n", frame_size);
  size_t size_ethertype = sizeof(uint16_t);
  size_t size_q_header = sizeof(qh);

  size_t header_siz_without_tag = sizeof(struct EthernetHeader) - size_ethertype;

  void *tagged_frame = malloc(frame_size + size_q_header);
  // copy header without ethertype field
  memcpy(tagged_frame, frame, header_siz_without_tag);

  // copy 802.1Q header
  memcpy(tagged_frame + header_siz_without_tag, &qh, size_q_header);

  // copy rest of frame, including ethertype field
  memcpy(tagged_frame + header_siz_without_tag + size_q_header,
         frame + header_siz_without_tag,
         frame_size - header_siz_without_tag);

  char *frame_buffer = malloc(60);
  for (int i = 0; i < 30; i++) {
    sprintf(frame_buffer + i * 2, "%02X", *(int *)(frame + i));
  }
  //printerr("%s", frame_buffer);

  //printerr("\n====\n");
  char *tagged_frame_buffer = malloc(60);
  for (int i = 0; i < 30; i++) {
    sprintf(tagged_frame_buffer + i * 2, "%02X", *(int *)(tagged_frame + i));
  }
  //printerr("%s", tagged_frame_buffer);

  return tagged_frame;
}

static void *untag_frame(const void *frame, size_t frame_size) {
  size_t size_q_header = sizeof(struct Q);
  size_t size_ethertype = sizeof(uint16_t);

  size_t header_siz_without_tag = sizeof(struct EthernetHeader) - size_ethertype;
  size_t header_siz_with_q_tag = header_siz_without_tag + size_q_header;

  void *untagged_frame = malloc(frame_size - size_q_header);

  // copy untagged_frame without 802.1Q fields
  memcpy(untagged_frame, frame, header_siz_without_tag);

  // skip header µnð 801.1Q fields, then copy rest of the frame
  memcpy(untagged_frame + header_siz_without_tag, frame + header_siz_with_q_tag, frame_size - header_siz_with_q_tag);

  return untagged_frame;
}

static void update_switching_table(struct EthernetHeader *eh, int16_t vlan, struct Interface *ifc) {
  if (in_table(&eh->src, vlan)) {
    struct SwitchingTableEntry *src_entry = find_in_table(&eh->src, vlan);

    // update port number if it has changed
    if (ifc->ifc_num != src_entry->port_no) {
      src_entry->port_no = ifc->ifc_num;
    }

    // update last-seen timestamp
    src_entry->ts = (unsigned long)time(NULL);
  } else {
    struct SwitchingTableEntry entry;
    entry.ts = (unsigned long)time(NULL);
    entry.macAddress = eh->src;
    entry.port_no = ifc->ifc_num;
    write_to_table(&entry, vlan);
  }
}

/**
 * Parse and process frame received on @a ifc.
 *
 * @param ifc interface we got the frame on
 * @param frame raw frame data
 * @param frame_size number of bytes in @a frame
 */
static void parse_frame(struct Interface *ifc, void *frame, size_t frame_size) {
  const uint8_t *framec = frame;
  struct EthernetHeader eh;

  if (frame_size < sizeof(eh)) {
    fprintf(stderr, "Malformed frame\n");
    return;
  }
  memcpy(&eh, frame, sizeof(eh));

  eh.tag = htons(eh.tag);

  //printerr("Interface=%s, Tag=%x, untagged_vlan=%d\n", ifc->ifc_name, eh.tag, ifc->untagged_vlan);

  void *outgoing_frame = frame;
  size_t outgoing_frame_size = frame_size;

  // Frame entered on an interface with an untagged VLAN
  if (ifc->untagged_vlan > NO_VLAN) {
    int16_t vlan = ifc->untagged_vlan;

    // The frame was not tagged
    if (eh.tag != Q_TAG) {
      //printerr("Untagged Frame on Untagged Interface, ifc_vlan=%d\n", vlan);

      update_switching_table(&eh, vlan, ifc);

      // Tag the frame
      outgoing_frame = tag_frame(vlan, frame, frame_size);
      outgoing_frame_size = frame_size + sizeof(struct Q);

      // Check if we already know the outgoing port.
      struct SwitchingTableEntry *entry = find_in_table(&eh.dst, vlan);
      if (entry != NULL) {
        struct Interface *dst_ifc = &gifc[entry->port_no - 1];

        // If the outgoing port is untagged, we send an untagged frame
        if (dst_ifc->untagged_vlan > NO_VLAN) {
          //printerr("Know: Send untagged frame\n");
          forward_to(dst_ifc, frame, frame_size);
          return;
        }

        // If the outgoing port is tagged, we send a tagged frame
        //printerr("Know: Send tagged frame\n");
        forward_to(dst_ifc, outgoing_frame, outgoing_frame_size);
        return;
      }

      // If we don't know the outgoing port, we broadcast it to the ports with the correct VLANs
      for (int i = 0; i < num_ifc; i++) {
        // Don't send it to the receiving interface
        if (compare_mac(&gifc[i].mac, &ifc->mac) == 0) {
          continue;
        }

        if (gifc[i].untagged_vlan == vlan) {
          //printerr("Unknown: Send untagged frame\n");
          forward_to(&gifc[i], frame, frame_size);
        } else if (has_vlan(&gifc[i], vlan)) {
          //printerr("Unknown: Send tagged frame\n");
          //printerr("VLAN: %d", vlan);
          forward_to(&gifc[i], outgoing_frame, outgoing_frame_size);
        }
      }

      return;
    }

    // return;

    // The frame was tagged
    struct Q qh;
    memcpy(&qh, frame + MAC_ADDR_SIZE * 2, sizeof(qh));
    qh.tpid = htons(qh.tpid);
    qh.tci = htons(qh.tci);
    int16_t frame_vlan = extract_vid(&qh);

    // Drop the frame if it has the wrong tag
    if (frame_vlan != vlan) {
      //printerr("Untagged Interface, Invalid VLAN-ID\n");
      return;
    }

    update_switching_table(&eh, vlan, ifc);

    if (in_table(&eh.dst, vlan)) {
      // forward to correct interface
      struct SwitchingTableEntry *dst_entry = find_in_table(&eh.dst, vlan);
      //printerr("untagged ifc, forward\n");
      forward_to(&gifc[dst_entry->port_no - 1], outgoing_frame, outgoing_frame_size);
    } else {
      //printerr("going to multicast\n");
      for (int i = 0; i < num_ifc; i++) {
        // if not the arrival interface and the correct tagged VLAN
        if (compare_mac(&gifc[i].mac, &ifc->mac) != 0 && contains(gifc[i].tagged_vlans, vlan)) {
          //printerr("untagged ifc, forward (multicast)\n");
          forward_to(&gifc[i], outgoing_frame, outgoing_frame_size);
        }
      }
    }

   //print("Invalid frame, shouldn't receive tagged on untagged ifc\n");
    return; // invalid frame, drop it
  }

  //
  // The frame was received on a tagged interface
  //

  // Drop the frame if it doesn't have a VLAN tag
  if (eh.tag != Q_TAG) {
    return;
  }

  // Get the Q_Header from the frame and extract the vlan id
  struct Q qh;
  memcpy(&qh, frame + MAC_ADDR_SIZE * 2, sizeof(qh));
  qh.tpid = htons(qh.tpid);
  qh.tci = htons(qh.tci);
  int16_t vlan = extract_vid(&qh);

  //printerr("Tagged Interface, VLAN-ID=%d\n", vlan);

  // Drop the frame if it has the wrong tag
  if (!has_vlan(ifc, vlan)) {
    //printerr("Tagged Interface, Invalid VLAN-ID\n");
    return;
  }

  update_switching_table(&eh, vlan, ifc);

  outgoing_frame = untag_frame(frame, frame_size);
  outgoing_frame_size = frame_size - sizeof(struct Q);

  // Check if we already know the outgoing port.
  struct SwitchingTableEntry *entry = find_in_table(&eh.dst, vlan);
  if (entry != NULL) {
    struct Interface *dst_ifc = &gifc[entry->port_no - 1];

    // If the outgoing port is untagged, we send an untagged frame
    if (dst_ifc->untagged_vlan > NO_VLAN) {
      //printerr("Know: Send untagged frame\n");
      forward_to(dst_ifc, outgoing_frame, outgoing_frame_size);
      return;
    }

    // If the outgoing port is tagged, we send a tagged frame
    //printerr("Know: Send tagged frame\n");
    forward_to(dst_ifc, frame, frame_size);
    return;
  }

  // If we don't know the outgoing port, we broadcast it to the ports with the correct VLANs
  for (int i = 0; i < num_ifc; i++) {
    // Don't send it to the receiving interface
    if (compare_mac(&gifc[i].mac, &ifc->mac) == 0)
      continue;

    if (gifc[i].untagged_vlan == vlan) {
      //printerr("Unknown: Send untagged frame\n");
      forward_to(&gifc[i], outgoing_frame, outgoing_frame_size);
    } else if (has_vlan(&gifc[i], vlan)) {
      //printerr("Unknown: Send tagged frame\n");
      forward_to(&gifc[i], frame, frame_size);
    }
  }
}

/**
 * Process frame received from @a interface.
 *
 * @param interface number of the interface on which we received @a frame
 * @param frame the frame
 * @param frame_size number of bytes in @a frame
 */
static void handle_frame(uint16_t interface, const void *frame, size_t frame_size) {
  if (interface > num_ifc)
    abort();
  parse_frame(&gifc[interface - 1], frame, frame_size);
}

/**
 * Handle control message @a cmd.
 *
 * @param cmd text the user entered
 * @param cmd_len length of @a cmd
 */
static void handle_control(char *cmd, size_t cmd_len) {
  cmd[cmd_len - 1] = '\0';
  fprintf(stderr, "Received command `%s' (ignored)\n", cmd);
}

/**
 * Handle MAC information @a mac
 *
 * @param ifc_num number of the interface with @a mac
 * @param mac the MAC address at @a ifc_num
 */
static void handle_mac(uint16_t ifc_num, const struct MacAddress *mac) {
  if (ifc_num > num_ifc)
    abort();
  gifc[ifc_num - 1].mac = *mac;
}

/**
 * Parse tagged interface specification found between @a start
 * and @a end.
 *
 * @param start beginning of tagged specification, with ':'
 * @param end end of tagged specification, should point to ']'
 * @param off interface offset for error reporting
 * @param ifc[out] what to initialize
 * @return 0 on success
 */
static int parse_tagged(const char *start, const char *end, int off, struct Interface *ifc) {
  char *spec;
  unsigned int pos;

  if (':' != *start) {
    fprintf(stderr, "Tagged definition for interface #%d lacks ':'\n", off);
    return 1;
  }
  start++;
  spec = strndup(start, end - start);
  if (NULL == spec) {
    perror("strndup");
    return 1;
  }
  pos = 0;
  for (const char *tok = strtok(spec, ","); NULL != tok; tok = strtok(NULL, ",")) {
    unsigned int tag;

    if (pos == MAX_VLANS) {
      fprintf(stderr, "Too many VLANs specified for interface #%d\n", off);
      free(spec);
      return 1;
    }
    if (1 != sscanf(tok, "%u", &tag)) {
      fprintf(stderr, "Expected number in tagged definition for interface #%d\n", off);
      free(spec);
      return 1;
    }
    if (tag > MAX_VLANS) {
      fprintf(stderr, "%u is too large for a 802.1Q VLAN ID (on interface #%d)\n", tag, off);
      free(spec);
      return 1;
    }
    ifc->tagged_vlans[pos++] = (int16_t)tag;
  }
  ifc->tagged_vlans[pos] = NO_VLAN;
  free(spec);
  return 0;
}

/**
 * Parse untagged interface specification found between @a start
 * and @a end.
 *
 * @param start beginning of tagged specification, with ':'
 * @param end end of tagged specification, should point to ']'
 * @param off interface offset for error reporting
 * @param ifc[out] what to initialize
 * @return 0 on success
 */
static int parse_untagged(const char *start, const char *end, int off, struct Interface *ifc) {
  char *spec;
  unsigned int tag;

  if (':' != *start) {
    fprintf(stderr, "Untagged definition for interface #%d lacks ':'\n", off);
    return 1;
  }
  start++;
  spec = strndup(start, end - start);
  if (NULL == spec) {
    perror("strndup");
    return 1;
  }
  if (1 != sscanf(spec, "%u", &tag)) {
    fprintf(stderr, "Expected number in untagged definition for interface #%d\n", off);
    free(spec);
    return 1;
  }
  if (tag > MAX_VLANS) {
    fprintf(stderr, "%u is too large for a 802.1Q VLAN ID (on interface #%d)\n", tag, off);
    free(spec);
    return 1;
  }
  ifc->untagged_vlan = (int16_t)tag;
  free(spec);
  return 0;
}

/**
 * Parse command-line argument with interface specification.
 *
 * @param arg command-line argument
 * @param off offset of @a arg for error reporting
 * @param ifc interface to initialize (ifc_name, tagged_vlans and
 * untagged_vlan).
 * @return 0 on success
 */
static int parse_vlan_args(const char *arg, int off, struct Interface *ifc) {
  const char *openbracket;
  const char *closebracket;

  ifc->tagged_vlans[0] = NO_VLAN;
  ifc->untagged_vlan = NO_VLAN;
  openbracket = strchr(arg, (unsigned char)'[');
  if (NULL == openbracket) {
    ifc->ifc_name = strdup(arg);
    if (NULL == ifc->ifc_name) {
      perror("strdup");
      return 1;
    }
    ifc->untagged_vlan = DEFAULT_VLAN;
    return 0;
  }
  ifc->ifc_name = strndup(arg, openbracket - arg);
  if (NULL == ifc->ifc_name) {
    perror("strndup");
    return 1;
  }
  openbracket++;
  closebracket = strchr(openbracket, (unsigned char)']');
  if (NULL == closebracket) {
    fprintf(stderr, "Interface definition #%d includes '[' but lacks ']'\n", off);
    return 1;
  }
  switch (*openbracket) {
  case 'T':
    return parse_tagged(openbracket + 1, closebracket, off, ifc);
    break;
  case 'U':
    return parse_untagged(openbracket + 1, closebracket, off, ifc);
    break;
  default:
    fprintf(stderr,
            "Unsupported tagged/untagged specification `%c' in interface "
            "definition #%d\n",
            *openbracket,
            off);
    return 1;
  }
}

#include "loop.c"

/**
 * Launches the vswitch.
 *
 * @param argc number of arguments in @a argv
 * @param argv binary name, followed by list of interfaces to switch between
 * @return not really
 */
int main(int argc, char **argv) {
  struct Interface ifc[argc - 1];

  memset(ifc, 0, sizeof(ifc));
  memset(switching_table, 0, sizeof(switching_table));
  num_ifc = argc - 1;
  gifc = ifc;
  for (unsigned int i = 1; i < argc; i++) {
    ifc[i - 1].ifc_num = i;
    if (0 != parse_vlan_args(argv[i], i, &ifc[i - 1]))
      return 1;
  }
  loop();
  return 0;
}
