#include "utils.h"

void print_mac(const struct MacAddress *mac) {
  print(
      "%02x:%02x:%02x:%02x:%02x:%02x", mac->mac[0], mac->mac[1], mac->mac[2], mac->mac[3], mac->mac[4], mac->mac[5]);
}

char* to_str_mac(const struct MacAddress *mac) {
    char* buf = malloc(MAC_ADDR_SIZE * 3);
    sprintf(buf, "%02x:%02x:%02x:%02x:%02x:%02x", mac->mac[0], mac->mac[1], mac->mac[2], mac->mac[3], mac->mac[4], mac->mac[5]);
    return buf;
}

void print_ip(const struct in_addr *ip) {
    char buf[INET_ADDRSTRLEN];
    print("%s", inet_ntop(AF_INET, ip, buf, sizeof(buf)));
}

char* to_str_ip(const struct in_addr *ip) {
    char inet_buf[INET_ADDRSTRLEN];
    char* buf = malloc(20);
    sprintf(buf, "%s", inet_ntop(AF_INET, ip, inet_buf, sizeof(inet_buf)));
    return buf;
}

void print_eh(const struct EthernetHeader *eh) {
    print("src=");
    print_mac(&eh->src);
    print(", dst=");
    print_mac(&eh->dst);
    print(", tag=%d\n", eh->tag);
}

void print_entry(const struct SwitchingTableEntry *entry) {
    print("mac_address=");
    print_mac(&entry->macAddress);
    print(", timestamp=%li", entry->ts);
    print(", port_no=%d\n", entry->port_no);
}

int compare_mac(const struct MacAddress *mac1, const struct MacAddress *mac2) {
  return memcmp(mac1, mac2, sizeof(struct MacAddress));
}

int compare_ip(const struct in_addr *ip1, const struct in_addr *ip2) {
    uint32_t s_addr1 = ip1->s_addr;
    uint32_t s_addr2 = ip2->s_addr;
    return s_addr1 == s_addr2 ? 0 : ( s_addr1 < s_addr2 ? -1 : 1 );
}