#ifndef SWITCHING_TABLE_ENTRY_H
#define SWITCHING_TABLE_ENTRY_H

#include "mac_address.h"
#include <stdint.h>

struct SwitchingTableEntry {
  unsigned long ts;
  struct MacAddress macAddress;
  uint16_t port_no;
};

#endif
