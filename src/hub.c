/*
  This file (was) part of GNUnet.
  Copyright (C) 2018 Christian Grothoff

  GNUnet is free software: you can redistribute it and/or modify it
  under the terms of the GNU Affero General Public License as published
  by the Free Software Foundation, either version 3 of the License,
  or (at your option) any later version.

  GNUnet is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file hub.c
 * @brief Stupidly forwards network traffic between interfaces
 * @author Christian Grothoff
 */
#include "glab.h"
#include "loop.c"
#include "utils.h"
#include "interface.h"
#include "mac_address.h"
#include "ethernet_header.h"

/**
 * Number of available contexts.
 */
static unsigned int number_of_interfaces;

/**
 * All the contexts.
 */
static struct Interface *gifc;

/**
 * Forward @a frame to interface @a dst.
 *
 * @param dst target interface to send the frame out on
 * @param frame the frame to forward
 * @param frame_size number of bytes in @a frame
 */
static void forward_to(struct Interface *dst, const void *frame, size_t frame_size) {
  char iob[frame_size + sizeof(struct GLAB_MessageHeader)];
  struct GLAB_MessageHeader hdr;

  hdr.size = htons(sizeof(iob));
  hdr.type = htons(dst->ifc_num);
  memcpy(iob, &hdr, sizeof(hdr));
  memcpy(&iob[sizeof(hdr)], frame, frame_size);

  write_all(STDOUT_FILENO, iob, sizeof(iob));
}

static void fwd_frame(struct Interface *src_ifc, const void *frame, size_t frame_size) {
  for (int i = 0; i < number_of_interfaces; i++) {
    if (compare_mac(&gifc[i].mac, &src_ifc->mac) != 0) {
        forward_to(&gifc[i], frame, frame_size);
    }
  }
}

/**
 * Process frame received from @a interface.
 *
 * @param interface number of the interface on which we received @a frame
 * @param frame the frame
 * @param frame_size number of bytes in @a frame
 */
static void handle_frame(uint16_t interface, const void *frame, size_t frame_size) {
  if (interface > number_of_interfaces)
    abort();
  fwd_frame(&gifc[interface - 1], frame, frame_size);
}

/**
 * Handle control message @a cmd.
 *
 * @param cmd text the user entered
 * @param cmd_len length of @a cmd
 */
static void handle_control(char *cmd, size_t cmd_len) {
  cmd[cmd_len - 1] = '\0';
  // print("Received command `%s' (ignored)\n", cmd);
}

/**
 * Handle MAC information @a mac
 *
 * @param ifc_num number of the interface with @a mac
 * @param mac the MAC address at @a ifc_num
 */
static void handle_mac(uint16_t ifc_num, const struct MacAddress *mac) {
  if (ifc_num > number_of_interfaces)
    abort();
  gifc[ifc_num - 1].mac = *mac;
}

int main(int argc, char **argv) {
  number_of_interfaces = argc - 1;
  struct Interface ifc[number_of_interfaces];
  memset(ifc, 0, sizeof(ifc));

  gifc = ifc;
  for (unsigned int i = 1; i <= number_of_interfaces; i++)
    ifc[i - 1].ifc_num = i;

  loop();
  return 0;
}
