#ifndef Q_H
#define Q_H

#include <stdint.h>

/**
 * IEEE 802.1Q header.
 */
struct Q {
    uint16_t tpid;
    uint16_t tci;
};

#define MAX_VLANS 4096
#define Q_TAG 0x8100

#endif
