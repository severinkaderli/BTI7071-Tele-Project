#ifndef ETHERNET_HEADER_H
#define ETHERNET_HEADER_H

#include "mac_address.h"
#include <stdint.h>

/**
 * An ethernet frame header
 */

struct EthernetHeader {
    struct MacAddress dst;
    struct MacAddress src;
    uint16_t tag;
};
#endif
