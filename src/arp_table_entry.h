#ifndef ARP_TABLE_ENTRY_H
#define ARP_TABLE_ENTRY_H

#include "mac_address.h"
#include <stdint.h>
#include <arpa/inet.h>

struct ARPTableEntry {
  unsigned long ts;
  struct MacAddress macAddress;
  struct in_addr ipAddress;
  uint16_t ifc_num;
};

#endif
