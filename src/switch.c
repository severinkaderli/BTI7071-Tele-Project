/*
     This file (was) part of GNUnet.
     Copyright (C) 2018 Christian Grothoff

     GNUnet is free software: you can redistribute it and/or modify it
     under the terms of the GNU Affero General Public License as published
     by the Free Software Foundation, either version 3 of the License,
     or (at your option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     Affero General Public License for more details.

     You should have received a copy of the GNU Affero General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file switch.c
 * @brief Ethernet switch
 * @author Christian Grothoff
 */
#include "ethernet_header.h"
#include "glab.h"
#include "interface.h"
#include "loop.c"
#include "mac_address.h"
#include "switching_table_entry.h"
#include "utils.h"
#include <stdbool.h>

/**
 * Number of available contexts.
 */
static unsigned int num_ifc;

/**
 * All the contexts.
 */
static struct Interface *gifc;

#define TABLE_SIZE 128
static struct SwitchingTableEntry switching_table[TABLE_SIZE];

/**
 * Forward @a frame to interface @a dst.
 *
 * @param dst target interface to send the frame out on
 * @param frame the frame to forward
 * @param frame_size number of bytes in @a frame
 */
static void forward_to(struct Interface *dst, const void *frame, size_t frame_size) {
  char iob[frame_size + sizeof(struct GLAB_MessageHeader)];
  struct GLAB_MessageHeader hdr;

  hdr.size = htons(sizeof(iob));
  hdr.type = htons(dst->ifc_num);
  memcpy(iob, &hdr, sizeof(hdr));
  memcpy(&iob[sizeof(hdr)], frame, frame_size);
  write_all(STDOUT_FILENO, iob, sizeof(iob));
}

static struct SwitchingTableEntry *find_in_table(struct MacAddress *mac) {
  for (int i = 0; i < TABLE_SIZE; i++) {
    if (compare_mac(mac, &switching_table[i].macAddress) == 0) {
      return &switching_table[i];
    }
  }
  return NULL;
}

static bool in_table(struct MacAddress *mac) {
  for (int i = 0; i < TABLE_SIZE; i++) {
    if (compare_mac(mac, &switching_table[i].macAddress) == 0) {
      return true;
    }
  }
  return false;
}

static void print_table() {
  print("-----------------------------------\n");
  for (int i = 0; i < TABLE_SIZE; i++) {
    print_entry(&switching_table[i]);
  }
}

static void write_to_table(struct SwitchingTableEntry *entry) {
  for (int i = 0; i < TABLE_SIZE; i++) {
    if (switching_table[i].port_no == 0) { // no entry at this location
      switching_table[i] = *entry;
      return;
    }
  }

  unsigned int oldest_index = 0;
  unsigned long oldest_time = 2147483648; // 2<<31, but the compiler is happy
  for (int i = 0; i < TABLE_SIZE; i++) {
    if (switching_table[i].port_no == 0) {
      if (switching_table[i].ts < oldest_time) {
        oldest_time = switching_table[i].ts;
        oldest_index = i;
      }
    }
  }
  switching_table[oldest_index] = *entry;
}

/**
 * Parse and process frame received on @a ifc.
 *
 * @param ifc interface we got the frame on
 * @param frame raw frame data
 * @param frame_size number of bytes in @a frame
 */
static void parse_frame(struct Interface *ifc, const void *frame, size_t frame_size) {
  struct EthernetHeader eh;

  if (frame_size < sizeof(eh)) {
    fprintf(stderr, "Malformed frame\n");
    return;
  }
  memcpy(&eh, frame, sizeof(eh));

  /*print("\n\n===============================\n\n");
  print("Souce: ");
  print_mac(&eh.src);
  print("\n");
  print("Destination: ");
  print_mac(&eh.dst);
  print("\n");
  print("Before:\n");
  print_table();*/

  if (in_table(&eh.src)) {
    struct SwitchingTableEntry *src_entry = find_in_table(&eh.src);

    // update port number if it has changed
    if(ifc->ifc_num != src_entry->port_no) {
      src_entry->port_no = ifc->ifc_num;
    }

    // update last-seen timestamp
    src_entry->ts = (unsigned long)time(NULL);
  } else {
    struct SwitchingTableEntry entry;
    entry.ts = (unsigned long)time(NULL);
    entry.macAddress = eh.src;
    entry.port_no = ifc->ifc_num;
    write_to_table(&entry);
  }

  if (in_table(&eh.dst)) {

    // forward to correct interface
    struct SwitchingTableEntry *dst_entry = find_in_table(&eh.dst);
    forward_to(&gifc[dst_entry->port_no - 1], frame, frame_size);
  } else {

    for (int i = 0; i < num_ifc; i++) {
      if (compare_mac(&gifc[i].mac, &ifc->mac) != 0) {
        forward_to(&gifc[i], frame, frame_size);
      }
    }
  }
}

/**
 * Process frame received from @a interface.
 *
 * @param interface number of the interface on which we received @a frame
 * @param frame the frame
 * @param frame_size number of bytes in @a frame
 */
static void handle_frame(uint16_t interface, const void *frame, size_t frame_size) {
  if (interface > num_ifc)
    abort();
  parse_frame(&gifc[interface - 1], frame, frame_size);
}

/**
 * Handle control message @a cmd.
 *
 * @param cmd text the user entered
 * @param cmd_len length of @a cmd
 */
static void handle_control(char *cmd, size_t cmd_len) {
  cmd[cmd_len - 1] = '\0';
  print("Received command `%s' (ignored)\n", cmd);
}

/**
 * Handle MAC information @a mac
 *
 * @param ifc_num number of the interface with @a mac
 * @param mac the MAC address at @a ifc_num
 */
static void handle_mac(uint16_t ifc_num, const struct MacAddress *mac) {
  if (ifc_num > num_ifc)
    abort();
  gifc[ifc_num - 1].mac = *mac;
}

/**
 * Launches the switch.
 *
 * @param argc number of arguments in @a argv
 * @param argv binary name, followed by list of interfaces to switch between
 * @return not really
 */
int main(int argc, char **argv) {
  struct Interface ifc[argc - 1];

  memset(ifc, 0, sizeof(ifc));
  num_ifc = argc - 1;
  gifc = ifc;
  for (unsigned int i = 1; i < argc; i++)
    ifc[i - 1].ifc_num = i;

  loop();
  return 0;
}
