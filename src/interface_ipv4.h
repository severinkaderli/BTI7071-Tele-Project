#ifndef INTERFACE_IPV4_H
#define INTERFACE_IPV4_H

#include "mac_address.h"
#include "q.h"

/**
 * Per-interface context.
 */
struct Interface {
    /**
     * MAC of interface.
     */
    struct MacAddress mac;

    /**
     * IPv4 address of interface (we only support one IP per interface!)
     */
    struct in_addr ip;

    /**
     * IPv4 netmask of interface.
     */
    struct in_addr netmask;

    /**
     * Name of the interface.
     */
    char *name;

    /**
     * Interface number.
     */
    uint16_t ifc_num;

    /**
     * MTU to enforce for this interface.
     */
    uint16_t mtu;
};
#endif
