#ifndef MAC_ADDRESS_H
#define MAC_ADDRESS_H

#include <stdint.h>

/**
 * Number of bytes in a MAC.
 */
#define MAC_ADDR_SIZE 6

/**
 * A MAC Address.
 */
struct MacAddress {
  uint8_t mac[MAC_ADDR_SIZE];
};

#endif
