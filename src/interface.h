#ifndef INTERFACE_H
#define INTERFACE_H

#include "mac_address.h"
#include "q.h"

/**
 * Per-interface context.
 */
struct Interface {
    /**
     * MAC of interface.
     */
    struct MacAddress mac;

    /**
     * Number of this interface.
     */
    uint16_t ifc_num;

    /**
     * Name of the network interface, i.e. "eth0".
     */
    char *ifc_name;

    /**
     * Which tagged VLANs does this interface participate in?
     * Array terminated by #NO_VLAN entry.
     */
    int16_t tagged_vlans[MAX_VLANS + 1];

    /**
     * Which untagged VLAN does this interface participate in?
     * #NO_VLAN for none.
     */
    int16_t untagged_vlan;
};

#endif
