#ifndef UTILS_H
#define UTILS_H

#include "glab.h"
#include "print.c"
#include "mac_address.h"
#include "ethernet_header.h"
#include "switching_table_entry.h"
#include <arpa/inet.h>

/**
 * Prints out a MAC address.
 *
 * @param mac The MAC address which will be printed.
 */
void print_mac(const struct MacAddress *mac);

char* to_str_mac(const struct MacAddress *mac);

/**
 * Prints out an IP address.
 * @param ip
 */
void print_ip(const struct in_addr *ip);

char* to_str_ip(const struct in_addr *ip);

void print_eh(const struct EthernetHeader *eh);

void print_entry(const struct SwitchingTableEntry *eh);

/**
 * Compares two MAC addresses.
 *
 * @param mac1 - The first MAC address
 * @param mac2 - The second MAC address
 * @return - 0 if the addresses are equal
 */
int compare_mac(const struct MacAddress *mac1, const struct MacAddress *mac2);


/**
 * Compares two IP Addresses.
 *
 * @return -  0 if the addresses are equal
 */
int compare_ip(const struct in_addr *ip1, const struct in_addr *ip2);

#endif
